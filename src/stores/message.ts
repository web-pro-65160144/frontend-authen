import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('messege', () => {
  const snackbar = ref(false)
  const text = ref('')
  function showMessage(msg: string) {
    text.value = msg
    snackbar.value = true
  }
  return { showMessage, snackbar, text }
})
